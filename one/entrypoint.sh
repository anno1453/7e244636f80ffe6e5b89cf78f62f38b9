#!/usr/bin/env bash

set -e

DO_CONNECTION_CHECK=${DO_CONNECTION_CHECK:-true}

if [ "${DO_CONNECTION_CHECK}" = true ]; then
    for link in $(env | grep _LINK= | cut -d = -f 2 | sort | uniq)
    do
        /app/wait-for-it.sh ${link} -t 0
    done
fi

if [ "$2" == 'runserver' ]; then
    /app/manage.py migrate
    /app/manage.py runserver 0.0.0.0:8000
fi

if [ "$2" == 'runworker' ]; then
    export DJANGO_SETTINGS_MODULE=one.settings
    /usr/local/bin/celery worker -A worker --loglevel=debug
fi
