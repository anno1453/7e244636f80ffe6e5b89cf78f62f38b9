from django.db import models
from django.core.validators import MinValueValidator
from django.utils.safestring import mark_safe

class FunctionTemplate(models.Model):
    formula = models.CharField(max_length=64, verbose_name='Формула', unique=True)

    class Meta:
        verbose_name = 'Встроенная функция'
        verbose_name_plural = 'Встроенные функции'

    def __str__(self):
        return self.formula


class Function(models.Model):
    template = models.ForeignKey(FunctionTemplate, related_name='functions', verbose_name='Функция', on_delete=models.CASCADE)
    interval = models.IntegerField(default=0, verbose_name='Интервал t, дней', validators=[MinValueValidator(1)])
    dt = models.IntegerField(default=0, verbose_name='Шаг t, часы', validators=[MinValueValidator(1)])
    updated_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата обработки', db_index=True)
    error = models.CharField(max_length=256, verbose_name='Ошибка', null=True, default='')

    chart = models.ImageField(blank=True, upload_to='user_media', verbose_name='График')

    def chart_tag(self):

        if self.error:
            return mark_safe('<font color="red">%s</font>' % self.error)
        return mark_safe('<img src="/media/%s" />' % self.chart)

    chart_tag.short_description = 'Image'
    chart_tag.allow_tags = True

    class Meta:
        verbose_name = 'Функция'
        verbose_name_plural = 'Функции'

    def __str__(self):
        return '{}, интервал = {}, шаг = {}'.format(self.template, self.interval, self.dt)
