from django.contrib import admin
from django.db.models import signals

from functions.models import FunctionTemplate, Function
from functions.tasks import update_function


class FunctionTemplateAdmin(admin.ModelAdmin):
    pass

admin.site.register(FunctionTemplate, FunctionTemplateAdmin)


def update_selected(admin_model, request, queryset):
    functions_ids = [obj.pk for obj in queryset]
    for function in Function.objects.filter(id__in=functions_ids).all():
        update_function.delay(function_id=function.id)

update_selected.short_description = "Обновить"


def notify_admin(sender, instance, created, **kwargs):
    if created:
        update_function(function_id=instance.id)


signals.post_save.connect(notify_admin, sender=Function)


class FunctionAdmin(admin.ModelAdmin):
    actions = [update_selected]

    list_display = ('template', 'chart_tag', 'interval', 'dt', 'updated_at')

    fields = ('chart_tag', 'template', 'interval', 'dt')
    readonly_fields = ('chart_tag',)


admin.site.register(Function, FunctionAdmin)
