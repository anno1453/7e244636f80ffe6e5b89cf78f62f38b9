# Generated by Django 2.1.3 on 2018-11-28 20:13

from django.db import migrations, models
from django.contrib.auth.models import User


def built_functions(apps, schema_editor):
    FunctionTemplate = apps.get_model("functions", "FunctionTemplate")

    for formula in ['sin(t)', 't-2/t']:
        if not FunctionTemplate.objects.filter(formula=formula).exists():
            FunctionTemplate(
                formula=formula
            ).save()


def default_user(apps, schema_editor):
    if not User.objects.filter(username='username').exists():
        user = User(
            pk=2,
            username="username",
            is_active=True,
            is_superuser=True,
            is_staff=True,
            email="username@mail.com",
        )
        user.set_password('password')
        user.save()


class Migration(migrations.Migration):

    dependencies = [
        ('functions', '0002_auto_20181128_1353'),
    ]

    operations = [
        migrations.RunPython(built_functions),
        migrations.RunPython(default_user),
    ]
