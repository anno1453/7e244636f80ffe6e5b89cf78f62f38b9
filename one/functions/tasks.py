import datetime
import logging
import requests
from celery import Celery

from django.conf import settings
from django.core.files.base import ContentFile

from functions.models import Function


logger = logging.getLogger(__name__)


app = Celery('tasks', backend='redis://redis', broker='pyamqp://guest@rabbitmq//')


@app.task
def update_function(function_id):
    function = None

    try:
        function = Function.objects.get(id=function_id)
        coordinates_url = settings.COORDINATES_URL

        data = {}
        data['formula'] = function.template.formula
        data['interval'] = function.interval
        data['dt'] = function.dt

        try:
            response = requests.post(coordinates_url, json=data)

        except requests.exceptions.ConnectionError:
            raise Exception('Ошибка вычиcления функции: невозможно подключится к сервису')

        if response.status_code != 200:
            message = ''
            try:
                data = response.json()
                message = data.get('message')
            except:
                pass
            msg = 'Ошибка вычиcления функции'
            if message:
                msg += ': ' + message
            raise Exception(msg)

        coordinates = response.json()

        three_link = 'http://' + settings.THREE_LINK

        data = {
            "infile": {
                "title": {"text": "График функции {}".format(function)},
                 "xAxis": {"categories": [str(x) for x, y in coordinates]},
                 "series": [{"data": [float(y) for x, y in coordinates]}]
              }
        }

        try:
            response = requests.post(three_link, json=data)

        except requests.exceptions.ConnectionError:
            raise Exception('Ошибка отрисовки функции: невозможно подключится к сервису')

        if response.status_code != 200:
            msg = 'Ошибка отрисовки функции'
            raise Exception(msg)

        content = response.content
        function.chart.save('chart.png', ContentFile(content))

        function.updated_at = datetime.datetime.now()
        function.error = None
        function.save()
        logger.info('Function update successed')

    except Exception as e:
        msg = '{}: {}'.format(type(e).__name__, str(e))
        logger.error(msg)

        if function is not None:
            function.error = msg[:255]
            function.save()

