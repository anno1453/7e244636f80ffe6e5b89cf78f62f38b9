"""two URL Configuration
"""

from django.contrib import admin
from django.urls import path

from two.views import function_coordinates


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/function/coordinates/', function_coordinates),
]
