import time

import django
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from rest_framework import serializers


FORMULA_CHOICES = [
    ('sin(t)', 'sin(t)', ),
    ('t-2/t', 't-2/t', )
]


class FunctionSerializer(serializers.Serializer):
    formula = serializers.ChoiceField(choices=FORMULA_CHOICES, default=FORMULA_CHOICES[0][0])
    interval = serializers.IntegerField(min_value=1)
    dt = serializers.IntegerField(min_value=1, max_value=23)


def execute_sql(sql, params):
    from django.db import connection

    cursor = connection.cursor()

    cursor.execute(sql, params)

    rows = cursor.fetchall()

    return rows


def calculate_function(formula, interval, dt):

    second_interval = (interval * 24 * 60 * 60)
    second_dt = dt * 60 * 60

    stop = int(time.time())
    step = second_dt
    start = stop - int(second_interval / second_dt) * second_dt

    if formula == 't-2/t':
        sql = f'SELECT t as x, t + 2 / t as y FROM generate_series({start}, {stop}, {step}) t; '
    elif formula == 'sin(t)':
        sql = f'SELECT t as x, sin(t) as y FROM generate_series({start}, {stop}, {step}) t; '
    else:
        raise Exception('Неизвестная функция')
    return execute_sql(sql, [])


@csrf_exempt
def function_coordinates(request):
    try:
        import json
        data = json.loads(request.body)

        serializer = FunctionSerializer(data=data)

        if not serializer.is_valid():
            raise Exception(str(serializer.errors))

        formula = data.get('formula')
        interval = int(data.get('interval'))
        dt = int(data.get('dt'))

        data = calculate_function(formula, interval, dt)

    except django.db.utils.DataError as e:
        data = {
            'success': False,
            'message': 'Ошибка вычисления на уровне базы данных {}'.format(e),
        }
        return JsonResponse(data, safe=False, status=400)

    except Exception as e:
        data = {
            'success': False,
            'message': str(e),
        }
        return JsonResponse(data, safe=False, status=400)

    return JsonResponse(data, safe=False)
