#!/usr/bin/env bash

set -e

/app/wait-for-it.sh ${DB_LINK} -t 0
/app/wait-for-it.sh ${RABBITMQ_LINK} -t 0

LOG_LEVEL='DEBUG'

/app/manage.py runserver 0.0.0.0:8000

